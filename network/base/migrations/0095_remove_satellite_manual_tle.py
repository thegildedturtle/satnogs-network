# Generated by Django 3.1.13 on 2022-01-12 11:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0094_migrate_artifact_files_from_old_to_new_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='satellite',
            name='manual_tle',
        ),
    ]
